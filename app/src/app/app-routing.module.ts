import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./pages/home/home.component";
import {RoleViewComponent} from "./pages/entities/role-view/role-view.component";
import {UserViewComponent} from "./pages/entities/user-view/user-view.component";
import {RoleInformationComponent} from "./elements/entity/tabs/role-information/role-information.component";
import {GroupViewComponent} from "./pages/entities/group-view/group-view.component";
import {UserInformationComponent} from "./elements/entity/tabs/user-information/user-information.component";
import {GroupInformationComponent} from "./elements/entity/tabs/group-information/group-information.component";
import {DisabledRolesComponent} from "./pages/entity-lists/disabled-roles/disabled-roles.component";
import {UnusedRolesComponent} from "./pages/entity-lists/unused-roles/unused-roles.component";
import {DisabledGroupsComponent} from "./pages/entity-lists/disabled-groups/disabled-groups.component";
import {EmptyGroupsComponent} from "./pages/entity-lists/empty-groups/empty-groups.component";
import {DeletedUsersComponent} from "./pages/entity-lists/deleted-users/deleted-users.component";
import {GroupVisualizationComponent} from "./elements/entity/tabs/group-visualization/group-visualization.component";
import {NotFoundComponent} from "./pages/not-found/not-found.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'disabled-roles',
    component: DisabledRolesComponent
  },
  {
    path: 'unused-roles',
    component: UnusedRolesComponent
  },
  {
    path: 'disabled-groups',
    component: DisabledGroupsComponent
  },
  {
    path: 'empty-groups',
    component: EmptyGroupsComponent
  },
  {
    path: 'deleted-users',
    component: DeletedUsersComponent
  },
  {
    path: 'role/:id',
    component: RoleViewComponent,
    children: [
      {
        path: '',
        component: RoleInformationComponent
      }
    ]
  },
  {
    path: 'user/:id',
    component: UserViewComponent,
    children: [
      {
        path: '',
        component: UserInformationComponent
      }
    ]
  },
  {
    path: 'group/:id',
    component: GroupViewComponent,
    children: [
      {
        path: '',
        component: GroupInformationComponent
      },
      {
        path: 'visualization',
        component: GroupVisualizationComponent
      }
    ]
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/not-found'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
