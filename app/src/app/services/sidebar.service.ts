import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * Represents a state of the sidebar (open/closed)
 */
export class SidebarService {

  sidebarOpen: boolean = false;

  constructor() { }

  /**
   * Opens/closes the sidebar
   */
  switchSidebar(): void {
    this.sidebarOpen = !this.sidebarOpen;
  }

  /**
   * Closes the sidebar
   */
  closeSidebar(): void {
    this.sidebarOpen = false;
  }

  /**
   * Returns the current state of the sidebar
   */
  isOpen(): boolean {
    return this.sidebarOpen;
  }

}
