import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Group} from "../../models/group.model";
import {ResourceService} from "./resource.service.interface";
import {Role} from "../../models/role.model";

@Injectable({
  providedIn: 'root'
})
/**
 * Handles getting groups
 */
export class GroupService implements ResourceService {

  constructor(private http: HttpClient) { }

  get(offset: number = null, limit: number = null, substring: string = ''): Observable<Group[]> {
    if (substring) {
      return this.http.get<Group[]>(`/api/groups/substring/${substring}/offset/${offset}/limit/${limit}`);
    }
    return this.http.get<Group[]>(`/api/groups/offset/${offset}/limit/${limit}`);
  }

  getDisabled() {
    return this.http.get<Group[]>('/api/groups/disabled');
  }

  getEmpty() {
    return this.http.get<Group[]>('/api/groups/empty');
  }

  getRolesForDescendantGroups(groupId: number): Observable<{id: Role[]}> {
    return this.http.get<{id: Role[]}>(`/api/group/${groupId}/roles-for-descendants`);
  }

  getById(id: number): Observable<Group> {
    return this.http.get<Group>(`/api/group/${id}`);
  }

  getByIdWithAllRelations(id: number): Observable<Group> {
    return this.http.get<Group>(`/api/group/${id}/all-relations`);
  }

}
