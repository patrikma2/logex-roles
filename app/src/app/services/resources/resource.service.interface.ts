import {Observable} from "rxjs";

/**
 * General resource service for getting users, roles, groups. It is used in search-result-block to pass
 * different services there.
 */
export interface ResourceService {
  /**
   * Gets a list of resources, with optional offset, limit and substring
   * @param offset - offset (null disables it)
   * @param limit - maximum count (null disables it)
   * @param substring - only resources containing this substring in relevant attributes will be searched
   * (null or empty string disables it)
   */
  get(offset: number, limit: number, substring: string) : Observable<any[]>
}
