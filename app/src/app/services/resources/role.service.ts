import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Role} from "../../models/role.model";

@Injectable({
  providedIn: 'root'
})
/**
 * Handles getting roles
 */
export class RoleService {

  constructor(private http: HttpClient) { }

  get(offset: number = null, limit: number = null, substring: string = ''): Observable<Role[]> {
    if (substring) {
      return this.http.get<Role[]>(`/api/roles/substring/${substring}/offset/${offset}/limit/${limit}`);
    }
    return this.http.get<Role[]>(`/api/roles/offset/${offset}/limit/${limit}`);
  }

  getDisabled(): Observable<Role[]> {
    return this.http.get<Role[]>('/api/roles/disabled');
  }

  getUnused(): Observable<Role[]> {
    return this.http.get<Role[]>('/api/roles/unused');
  }

  getById(id: number): Observable<Role> {
    return this.http.get<Role>(`/api/role/${id}`);
  }

  getByIdWithAllRelations(id: number): Observable<Role> {
    return this.http.get<Role>(`/api/role/${id}/all-relations`);
  }

}
