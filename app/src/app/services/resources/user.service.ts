import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from '@angular/common/http';
import {User} from "../../models/user.model";
import {Role} from "../../models/role.model";

@Injectable({
  providedIn: 'root'
})
/**
 * Handles getting users
 */
export class UserService {

  constructor(private http: HttpClient) { }

  get(offset: number = null, limit: number = null, substring: string = ''): Observable<User[]> {
    if (substring) {
      return this.http.get<User[]>(`/api/users/substring/${substring}/offset/${offset}/limit/${limit}`);
    }
    return this.http.get<User[]>(`/api/users/offset/${offset}/limit/${limit}`);
  }

  getDeleted(): Observable<User[]> {
    return this.http.get<User[]>('/api/users/deleted');
  }

  getById(id: number): Observable<User> {
    return this.http.get<User>(`/api/user/${id}`);
  }

  getByIdWithAllRelations(id: number): Observable<User> {
    return this.http.get<User>(`/api/user/${id}/all-relations`);
  }

}
