import { Injectable } from '@angular/core';
import {VisitedEntity} from "../models/visited-entity.model";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  /**
   * Returns visited entities from local storage
   */
  getVisitedItems(): VisitedEntity[] {
    return JSON.parse(localStorage.getItem('visitedItems'));
  }

  /**
   * Saves visit to the local storage. If there are 5 visits, the oldest one is removed
   * @param type
   * @param name
   * @param id
   */
  saveVisitedItem(type: string, name: string, id: number) {
    let currentArray: VisitedEntity[] = this.getVisitedItems();
    if (!currentArray) {
      currentArray = [];
    }
    currentArray = this.removeSameVisits(type, name, id, currentArray);
    currentArray = this.updateVisitedItems(type, name, id, currentArray);
    localStorage.setItem('visitedItems', JSON.stringify(currentArray));
  }

  /**
   * Removes entities with the given attributes from the array of visits
   * @param list
   * @param type
   * @param name
   * @param id
   */
  private removeSameVisits(type: string, name: string, id: number, list: VisitedEntity[]) : VisitedEntity[] {
    return list.filter(item => {
      return (item.name !== name) || (item.id !== id) || (item.type !== type);
    });
  }

  /**
   * Enqueues the newest visit, if there are already 5 visits, the oldest visit is removed
   * @param newItemType
   * @param newItemName
   * @param newItemId
   * @param list
   */
  private updateVisitedItems(newItemType: string, newItemName: string, newItemId: number, list: VisitedEntity[]) {
    if (list.length >= 5) {
      list.pop();
    }
    list.unshift({
      type: newItemType,
      name: newItemName,
      id: newItemId
    });
    return list;
  }
}
