import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";

/**
 * Service used for transferring data from view pages to corresponding router-outlets
 */
@Injectable({
  providedIn: 'root'
})
export class OutletTransferService<T> {

  private resource: Subject<T> = new BehaviorSubject<T>(null);

  constructor() { }

  /**
   * Gets the observable stream
   */
  get stream(): Observable<T> {
    return this.resource.asObservable();
  }

  /**
   * Saves the resource
   * @param newResource
   */
  send(newResource: T) {
    this.resource.next(newResource);
  }

}
