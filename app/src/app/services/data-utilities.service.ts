import { Injectable } from '@angular/core';
import {User} from "../models/user.model";
import {Role} from "../models/role.model";
import {Group} from "../models/group.model";

@Injectable({
  providedIn: 'root'
})
export class DataUtilitiesService {

  constructor() { }

  /**
   * Unions two arrays of entities and adds information about the array it came from (direct/acquired)
   * @param direct - directly assigned roles/users
   * @param acquired - roles/users assigned through groups
   * @param primaryKey - ID of the entity
   * @param sort - if final array should be sorted
   */
  unionEntities(direct: User[] | Role[] | Group[],
                acquired: User[] | Role[] | Group[],
                primaryKey: string,
                sort: boolean = true) {
    let union = [];
    direct.forEach(entity => {
      entity.directly = true;
      entity.acquired = false;
      union.push(entity);
    });
    acquired.forEach(entity => {
      const index = union.findIndex(e => e[primaryKey] === entity[primaryKey]);
      if (index === -1) {
        entity.acquired = true;
        entity.directly = false;
        union.push(entity);
        return;
      }
      union[index].acquired = true;
    });
    if (sort) {
      union.sort((a, b) => a[primaryKey] - b[primaryKey]);
    }
    return union;
  }

}
