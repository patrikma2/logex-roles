import { Injectable } from '@angular/core';
import {Group} from "../models/group.model";
import {GroupNode} from "../models/group-node.model";

@Injectable({
  providedIn: 'root'
})
export class HierarchyService {

  constructor() { }

  /**
   * Creates a hierarchy tree from a list of groups, each node has a children array, root node contains
   * depth attribute indicating the depth of the whole tree
   * @param rootGroup - root node, will be copied
   * @param descendants - list of descendants
   * @return root node with children in array and with total depth
   */
  createGroupHierarchy(rootGroup: Group, descendants: Group[]): GroupNode {
    const root: GroupNode = this.convertToGroupNode(rootGroup);
    let groupParentsMap = this.createParentsMap(descendants);
    const allNodes: GroupNode[] = [root].concat(descendants);
    allNodes.forEach(node => {
      if (!groupParentsMap.has(node.id)) {
        node.children = [];
        return;
      }
      node.children = groupParentsMap.get(node.id);
    });
    root.depth = groupParentsMap.size + 1;
    return root;
  }

  /**
   * Copies object with its required properties for GroupNode
   * @param group
   */
  convertToGroupNode(group: Group): GroupNode {
    return {
      id: group.id,
      uid: group.uid,
      group_description: group.group_description,
      parent_group_id: group.parent_group_id,
      product_id: group.product_id,
      roles: group.roles_direct
    }
  }

  /**
   * Creates a map with parent_group_id as a key and array of groups with the same parent as value
   * @param groups - list of group which will be used
   */
  private createParentsMap(groups: Group[]): Map<number, GroupNode[]> {
    let parentMap = new Map<number, GroupNode[]>();
    groups.forEach(group => {
      if (parentMap.has(group.parent_group_id)) {
        parentMap.get(group.parent_group_id).push(group);
        return;
      }
      parentMap.set(group.parent_group_id, [group]);
    });
    return parentMap;
  }

}
