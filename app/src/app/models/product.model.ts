export class Product {
  id: number;
  uid: string;
  product_description: string;
  abbreviation: string;
  role_prefix: string;
}
