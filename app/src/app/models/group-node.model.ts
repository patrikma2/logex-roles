import {Role} from "./role.model";

export class GroupNode {
  id: number;
  uid: string;
  group_description: string;
  parent_group_id: number;
  product_id: number;

  children?: GroupNode[];
  roles?: Role[];
  depth?: number;
}
