import {Product} from "./product.model";
import {Role} from "./role.model";
import {User} from "./user.model";

export class Group {
  // Main info
  id: number;
  uid: string;
  group_description: string;
  parent_group_id: number;
  product_id: number;

  // Optional info
  product?: Product;
  descendant_groups?: Group[];
  sibling_groups?: Group[];
  ancestor_groups?: Group[];
  roles_direct?: Role[];
  roles_inherited?: Role[];
  users_direct?: User[];
  users_inherited?: User[];

  // For tables with assignment type
  directly?: boolean;
  acquired?: boolean;
}
