import {Group} from "./group.model";
import {User} from "./user.model";
import {Product} from "./product.model";

export class Role {
  id: number;
  uid: string;
  role_description: string;
  product_id: number;

  product?: Product;
  groups_direct?: Group[];
  groups_inherited?: Group[];
  users_direct?: User[];
  users_through_groups?: User[];

  directly?: boolean;
  acquired?: boolean;
}
