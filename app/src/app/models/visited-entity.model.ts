export class VisitedEntity {
  type: string;
  name: string;
  id: number;
}
