import {Group} from "./group.model";
import {Role} from "./role.model";

export class User {
  user_id: number;
  login: string;
  title: string;
  first_name: string;
  last_name: string;
  email: string;

  groups_direct?: Group[];
  groups_inherited?: Group[];
  roles_direct?: Role[];
  roles_inherited?: Role[];

  directly?: boolean;
  acquired?: boolean;
}
