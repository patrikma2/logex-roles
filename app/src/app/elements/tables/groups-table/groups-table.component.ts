import {Component, Input, OnInit} from '@angular/core';
import {Group} from "../../../models/group.model";
import {TableComponent} from "../table/table.component";

@Component({
  selector: 'logex-groups-table',
  templateUrl: './groups-table.component.html',
  styleUrls: ['./groups-table.component.scss']
})
export class GroupsTableComponent extends TableComponent implements OnInit {
  @Input() set groups(list: Group[]) {
    this.allGroups = list;
    this.shownGroups = list;
  }
  @Input() showAssignmentColumn: boolean = true;
  @Input() noResultsMessage: string = 'No groups found';
  @Input() acquiredIconExplanation: string = 'Descendent group (assigned through inheritance)';

  allGroups: Group[];
  searchedGroup: string = '';
  shownGroups: Group[];

  constructor() {
    super();
  }

  ngOnInit(): void { }

  updateResults() {
    if (!this.searchedGroup) {
      this.shownGroups = this.allGroups;
    }
    this.shownGroups = this.allGroups.filter(group => group.uid.includes(this.searchedGroup));
  }
}
