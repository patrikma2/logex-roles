import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tab-assignment-icon',
  templateUrl: './assignment-icon.component.html',
  styleUrls: ['./assignment-icon.component.scss']
})
export class AssignmentIconComponent implements OnInit {

  @Input() letter: string;
  @Input() explanation: string;

  constructor() { }

  ngOnInit(): void {
  }

}
