import {Component, Input, OnInit} from '@angular/core';
import {GroupsTableComponent} from "../groups-table/groups-table.component";

@Component({
  selector: 'logex-groups-mini-table',
  templateUrl: './groups-mini-table.component.html',
  styleUrls: ['./groups-mini-table.component.scss']
})
export class GroupsMiniTableComponent extends GroupsTableComponent implements OnInit {

  @Input() height: number = null;
  @Input() noResultsMessage: string = 'No results found';

  constructor() {
    super();
  }

  ngOnInit(): void {
  }
}
