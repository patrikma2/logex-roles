import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'logex-table-description',
  templateUrl: './table-description.component.html',
  styleUrls: ['./table-description.component.scss']
})
export class TableDescriptionComponent implements OnInit {

  @Input() text: string;

  constructor() { }

  ngOnInit(): void {
  }

}
