import {Component, Input, OnInit} from '@angular/core';
import {Role} from "../../../models/role.model";
import {TableComponent} from "../table/table.component";

@Component({
  selector: 'logex-roles-table',
  templateUrl: './roles-table.component.html',
  styleUrls: ['./roles-table.component.scss']
})
export class RolesTableComponent extends TableComponent implements OnInit {
  @Input() set roles(list: Role[]) {
    this.allRoles = list;
    this.shownRoles = list;
  }
  @Input() showAssignmentColumn: boolean = true;
  @Input() noResultsMessage: string = 'No roles found';
  @Input() acquiredIconExplanation: string = 'Acquired through group inheritance';

  allRoles: Role[];
  searchedRole: string = '';
  shownRoles: Role[];

  constructor() {
    super()
  }

  ngOnInit(): void { }

  updateResults() {
    if (!this.searchedRole) {
      this.shownRoles = this.allRoles;
    }
    this.shownRoles = this.allRoles.filter(role => role.uid.includes(this.searchedRole));
  }

}
