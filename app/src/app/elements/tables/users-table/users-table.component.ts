import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../models/user.model";
import {TableComponent} from "../table/table.component";

@Component({
  selector: 'logex-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent extends TableComponent implements OnInit {
  @Input() set users(list: User[]) {
    this.allUsers = list;
    this.shownUsers = list;
  }
  @Input() showAssignmentColumn: boolean = true;
  @Input() noResultsMessage: string = 'No users found';
  @Input() acquiredIconExplanation: string = 'Acquired through a group or its ancestor';

  allUsers: User[];
  searchedUser: string = '';
  shownUsers: User[];

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

  updateResults() {
    if (!this.searchedUser) {
      this.shownUsers = this.allUsers;
    }
    const normalizedText = this.searchedUser
      .normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    this.shownUsers = this.allUsers.filter(user => user.first_name.includes(normalizedText) ||
      user.last_name.includes(normalizedText) || (user.login.includes(normalizedText)))
  }
}
