import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'logex-table',
  template: ''
})
export class TableComponent implements OnInit {

  maximumNumberOfItems: number = 11;
  headerSize: number = 36;
  borderSize: number = 2;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Computes table the height to show the maximum number of results + header, the rest will be available
   * by scrolling
   * @param numberOfItems - number of all loaded items
   * @param isAssignmentIconDisplayed - if any row contains assignment icon
   */
  computeTableHeight(numberOfItems: number, isAssignmentIconDisplayed: boolean = true): string {
    const itemSize = isAssignmentIconDisplayed ? 39 : 38;
    if (numberOfItems >= this.maximumNumberOfItems) {
      return this.maximumNumberOfItems * itemSize + this.headerSize + this.borderSize + 'px';
    }
    return numberOfItems * itemSize + this.headerSize + this.borderSize + 'px';
  }
}
