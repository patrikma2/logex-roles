import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input, OnDestroy,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {Tab} from "../../../models/tab.model";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'logex-view-menu',
  templateUrl: './view-menu.component.html',
  styleUrls: ['./view-menu.component.scss']
})
export class ViewMenuComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() tabs: Tab[];
  @Input() selectedId: number = 0;

  @ViewChildren('tabItem' ) tabItems!:QueryList<ElementRef>;

  tabClicked: boolean = false;
  selectorWidth: number = 0;
  selectorOffset: number = 0;
  routeObserver = null;

  constructor(private cdr: ChangeDetectorRef, private router: Router) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.moveSelectorToCurrentLink();
    this.detectUrlChange();
  }

  ngOnDestroy() {
    this.routeObserver.unsubscribe();
  }

  /**
   * Sets width and X offset from the parent
   * @param id - ID of the link where the selection bar will be moved
   * @param fromUser - if selection was made by user, setting it to true enables animation
   */
  moveSelectorToLinkWithId(id: number, fromUser= false) {
    const selectedTabEl = this.tabItems.toArray()[id].nativeElement;
    this.selectorOffset = selectedTabEl.offsetLeft;
    this.selectorWidth = selectedTabEl.offsetWidth;
    if (fromUser) {
      this.tabClicked = true;
    }
    this.cdr.detectChanges();
  }

  /**
   * Moves the selection bar to the current rab (depending on the active url)
   * @param fromUser - if selection was made by user, setting it to true enables animation
   */
  moveSelectorToCurrentLink(fromUser: boolean = false) {
    const currentTabLink = this.router.url.split('/')[3];
    let currentId = this.tabs.findIndex(tab => tab.link === currentTabLink);
    if (currentId < 0) {
      currentId = 0;
    }
    this.moveSelectorToLinkWithId(currentId, fromUser)
  }

  /**
   * Creates an observer detecting url changes to move the selection bar accordingly
   */
  detectUrlChange() {
    this.routeObserver = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.moveSelectorToCurrentLink(true);
      }
    });
  }

}
