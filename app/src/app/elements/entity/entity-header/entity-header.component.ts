import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'logex-entity-header',
  templateUrl: './entity-header.component.html',
  styleUrls: ['./entity-header.component.scss']
})
export class EntityHeaderComponent implements OnInit {
  @Input() type: string;
  @Input() name: string;

  constructor() { }

  ngOnInit(): void {
  }

}
