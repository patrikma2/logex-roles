import {Component, Input, OnInit} from '@angular/core';
import {Role} from "../../../../models/role.model";
import {User} from "../../../../models/user.model";
import {DataUtilitiesService} from "../../../../services/data-utilities.service";
import {Group} from "../../../../models/group.model";
import {OutletTransferService} from "../../../../services/outlet-transfer.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'logex-role-information',
  templateUrl: './role-information.component.html',
  styleUrls: ['./role-information.component.scss']
})
export class RoleInformationComponent implements OnInit {

  role: Role = null;

  allUsers: User[];
  allGroups: Group[];

  subscription: Subscription = null;

  constructor(private dataUtilitiesService: DataUtilitiesService,
              private outletTransferService: OutletTransferService<Role>) {
    this.subscription = outletTransferService.stream.subscribe(role => {
      this.role = role;
      this.unionUsers();
      this.unionGroups();
    })
  }

  ngOnInit(): void { }

  unionUsers() {
    this.allUsers = this.dataUtilitiesService.unionEntities(this.role.users_direct,
      this.role.users_through_groups, 'user_id');
  }

  unionGroups() {
    this.allGroups = this.dataUtilitiesService.unionEntities(this.role.groups_direct,
      this.role.groups_inherited, 'id', false);
  }

  ngOnDestroy(): void {
    if (this.subscription !== null) {
      this.subscription.unsubscribe();
    }
  }

}
