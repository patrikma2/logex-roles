import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../../models/user.model";
import {Role} from "../../../../models/role.model";
import {DataUtilitiesService} from "../../../../services/data-utilities.service";
import {Group} from "../../../../models/group.model";
import {OutletTransferService} from "../../../../services/outlet-transfer.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'logex-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.scss']
})
export class UserInformationComponent implements OnInit {

  user: User;

  allRoles: Role[];
  allGroups: Group[];

  subscription: Subscription = null;

  constructor(private dataUtilitiesService: DataUtilitiesService,
              private outletTransferService: OutletTransferService<User>) {
    this.subscription = outletTransferService.stream.subscribe(user => {
      this.user = user;
      this.unionRoles();
      this.unionGroups();
    })
  }

  ngOnInit(): void {
  }

  unionRoles() {
    this.allRoles = this.dataUtilitiesService.unionEntities(this.user.roles_direct,
      this.user.roles_inherited, 'id')
  }

  unionGroups() {
    this.allGroups = this.dataUtilitiesService.unionEntities(this.user.groups_direct,
      this.user.groups_inherited, 'id', false);
  }

  ngOnDestroy(): void {
    if (this.subscription !== null) {
      this.subscription.unsubscribe();
    }
  }

}
