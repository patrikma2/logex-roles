import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Group} from "../../../../models/group.model";
import {DataUtilitiesService} from "../../../../services/data-utilities.service";
import {Role} from "../../../../models/role.model";
import {User} from "../../../../models/user.model";
import {OutletTransferService} from "../../../../services/outlet-transfer.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'logex-group-information',
  templateUrl: './group-information.component.html',
  styleUrls: ['./group-information.component.scss']
})
export class GroupInformationComponent implements OnInit, OnDestroy {

  group: Group = null;

  allRoles: Role[];
  allUsers: User[];

  subscription: Subscription = null;

  constructor(private dataUtilitiesService: DataUtilitiesService,
              private outletTransferService: OutletTransferService<Group>) {
    this.subscription = outletTransferService.stream.subscribe(group => {
      this.group = group;
      this.unionRoles();
      this.unionUsers();
    })
  }

  ngOnInit(): void {

  }

  unionRoles() {
    this.allRoles = this.dataUtilitiesService.unionEntities(this.group.roles_direct,
      this.group.roles_inherited, 'id', false)
  }

  unionUsers() {
    this.allUsers = this.dataUtilitiesService.unionEntities(this.group.users_direct,
      this.group.users_inherited, 'user_id', false);
  }

  ngOnDestroy(): void {
    if (this.subscription !== null) {
      this.subscription.unsubscribe();
    }
  }

}
