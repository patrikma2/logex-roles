import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {HierarchyService} from "../../../../services/hierarchy.service";
import {GroupsTreeComponent} from "../../../visualizations/groups-tree/groups-tree.component";
import {GroupNode} from "../../../../models/group-node.model";
import {Group} from "../../../../models/group.model";
import {GroupService} from "../../../../services/resources/group.service";
import {OutletTransferService} from "../../../../services/outlet-transfer.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'logex-group-visualization',
  templateUrl: './group-visualization.component.html',
  styleUrls: ['./group-visualization.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupVisualizationComponent implements OnInit {

  @ViewChild(GroupsTreeComponent) groupsTree: GroupsTreeComponent;

  showTree: boolean = true;
  defaultGroup: Group;
  descendants: boolean = true;
  rootGroup: GroupNode = null;
  rolesShown: boolean = false;

  subscription: Subscription = null;

  constructor(private hierarchyService: HierarchyService,
              private groupService: GroupService,
              private cdr: ChangeDetectorRef,
              private outletTransferService: OutletTransferService<Group>) {
    this.subscription = outletTransferService.stream.subscribe(group => {
      this.defaultGroup = group;
      this.setDescendantsTree();
    })
  }

  ngOnInit(): void { }

  /**
   * Shows/hides roles
   */
  switchRoles() {
    this.rolesShown = !this.rolesShown;
  }

  /**
   * Focuses current group
   */
  focusGroup() {
    this.groupsTree.focusGroup(this.defaultGroup.id, true);
  }

  /**
   * Opens tree in fullscreen
   */
  openFullscreen() {
    this.groupsTree.openFullScreen();
  }

  /**
   * Switches tree of descendants to a tree of ancestors and vice versa
   */
  switchTreeType() {
    if (this.descendants) {
      this.setAncestorsTree();
    } else {
      this.setDescendantsTree();
    }
    this.reloadTree();
  }

  /**
   * Creates a hierarchy for group descendants
   */
  private setDescendantsTree() {
    this.rootGroup = this.hierarchyService.createGroupHierarchy(this.defaultGroup, this.defaultGroup.descendant_groups);
    this.descendants = true;
  }

  /**
   * Creates a hierarchy for group ancestors
   */
  private setAncestorsTree() {
    if (this.defaultGroup.ancestor_groups.length === 0) {
      this.rootGroup = this.hierarchyService.createGroupHierarchy(this.defaultGroup, []);
      this.descendants = false;
      return;
    }
    const ancestorRoot = this.defaultGroup.ancestor_groups.find(group => group.parent_group_id === null);
    const remainingAncestors = this.defaultGroup.ancestor_groups.filter(group => group.parent_group_id !== null)
      .concat(this.hierarchyService.convertToGroupNode(this.defaultGroup));
    this.rootGroup = this.hierarchyService.createGroupHierarchy(ancestorRoot, remainingAncestors);
    this.descendants = false;
  }

  /**
   * Forces tree reload
   */
  private reloadTree() {
    this.showTree = false;
    this.cdr.detectChanges();
    this.showTree = true;
    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    if (this.subscription !== null) {
      this.subscription.unsubscribe();
    }
  }

}
