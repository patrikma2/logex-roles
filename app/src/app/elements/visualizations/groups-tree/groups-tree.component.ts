import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {hierarchy} from "d3-hierarchy";
import * as d3 from "d3";
import {GroupService} from "../../../services/resources/group.service";
import {Router} from "@angular/router";
import {HierarchyService} from "../../../services/hierarchy.service";
import {GroupNode} from "../../../models/group-node.model";

@Component({
  selector: 'logex-groups-tree',
  templateUrl: './groups-tree.component.html',
  styleUrls: ['./groups-tree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GroupsTreeComponent implements OnInit, OnChanges {

  @Input() rootGroup: GroupNode = null;
  @Input() showDirectRoles = false;
  @Input() currentGroupId: number = null;

  @ViewChild('canvasWrapper') canvasWrapper: ElementRef;
  @ViewChild('tree') groupsTree: ElementRef;

  // General attributes
  canvas: HTMLElement = null;
  hierarchyRoot: any = null;
  nodesSelection: d3.Selection<SVGElement, any, any, any> = null;
  treeSelection: d3.Selection<SVGElement, any, any, any> = null;

  // Used for displaying associated roles
  rolesLoaded: boolean = false;
  rolesMap: {} = null;

  // Used for displaying roles modal
  rolesModalGroupId: number = null;
  rolesModalUid: string = null;

  zoomed = ({transform}) => {
    d3.select(this.canvas).select('g').attr('transform', transform);
  };

  constructor(private groupService: GroupService,
              private hierarchyService: HierarchyService,
              private router: Router,
              private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    if (this.currentGroupId === null) {
      this.currentGroupId = this.rootGroup.id;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['showDirectRoles'].isFirstChange()){
      return;
    }
    if (this.showDirectRoles) {
      if (this.rolesLoaded) {
        this.appendRolesInfo();
        return;
      }
      this.addRolesToGroup(() => {
        this.appendRolesInfo();
      });
      return;
    }
    this.removeRolesInfo();
  }

  ngAfterViewInit() {
    this.createGroupTree();
    if (this.showDirectRoles) {
      this.addRolesToGroup(() => {
        this.appendRolesInfo();
      });
    }
  }

  /**
   * Moves group node with the given ID to canvas center
   * @param idToFocus - ID of the group
   * @param withTransition - turns on animation
   */
  focusGroup(idToFocus: number, withTransition: boolean = false) {
    const canvasSize = this.canvas.getBoundingClientRect();
    const elementToFocus: any = d3.select('#group-' + idToFocus).node();
    const [distanceToCenterX, distanceToCenterY] = this.calculateDistanceToCenter(elementToFocus);
    if (withTransition) {
      this.transformTreeWithTransition(distanceToCenterX, distanceToCenterY);
      return;
    }
    this.transformTree(distanceToCenterX, distanceToCenterY);
  }

  /**
   * Places the canvas to fullscreen and focuses the root group
   */
  openFullScreen() {
    if (this.canvasWrapper.nativeElement.requestFullscreen) {
      this.canvas.classList.add('fullscreen');
      this.canvasWrapper.nativeElement.requestFullscreen().then(() => {
        this.focusGroup(this.currentGroupId, true);
        document.addEventListener('fullscreenchange', this.detectFullscreenExit.bind(this));
      })
    }
  }

  /**
   * Initializes hierarchy and creates the whole tree (only with groups; roles etc. should be added later)
   */
  private createGroupTree() {
    this.hierarchyRoot = hierarchy(this.rootGroup);
    this.canvas = this.groupsTree.nativeElement;
    this.treeSelection = d3.select(this.canvas).select('.tree');
    this.createNodes(this.createTreeLayout());
    this.createLinks();
    this.enableZoom();
    this.focusGroup(this.currentGroupId);
  }

  /**
   * Initializes tree layout, minimum width is set by number of leaves
   */
  private createTreeLayout() {
    const treeLayout = d3.tree();
    const canvasWidth: number = this.canvas.getBoundingClientRect().width;
    const treeWidth: number = this.hierarchyRoot.leaves().length * 180;
    const treeHeight: number = this.rootGroup.depth * 210;
    treeLayout.size([Math.max(canvasWidth, treeWidth),
      treeHeight]);
    return treeLayout(this.hierarchyRoot);
  }

  /**
   * Creates tree nodes with group uids from previously set set hierarchy
   * @param tree - tree layout
   */
  private createNodes(tree) {
    this.nodesSelection = this.treeSelection
      .select('.nodes')
      .selectAll('.node')
      .data(this.hierarchyRoot.descendants())
      .enter()
      .append('g')
      .attr('id', (d: any) => 'group-' + d.data.id)
      .classed('node', true)
      .attr('x', (d: any) => d.x)
      .attr('y', (d: any) => d.y);

    this.nodesSelection.append('circle')
      .classed('node-circle', true)
      .attr('cx', (d: any) => d.x)
      .attr('cy', (d: any) => d.y)
      .attr('r', 5);

    this.nodesSelection.append('text')
      .attr('dx', (d: any) => d.x)
      .attr('dy', (d: any) => d.y + 25)
      .attr('data-id', (d: any) => d.data.id)
      .text((d: any) => d.data.uid)
      .classed('group-name', true)
      .style('text-anchor', 'middle')
      .on('click', (d) => this.router.navigate(['group', d.target.dataset.id]));
  }

  /**
   * Connects nodes added to tree
   */
  private createLinks() {
    const link = d3.linkVertical()
      .source((d: any) => [d.source.x, d.source.y])
      .target((d: any) => [d.target.x, d.target.y]);

    d3.select(this.canvas)
      .select('g.links')
      .selectAll('line.link')
      .data(this.hierarchyRoot.links())
      .enter()
      .append('path')
      .classed('link', true)
      .attr("d", link);
  }

  /**
   * Adds directly assigned roles to groups which have at least one such role
   */
  private appendRolesInfo() {
    const rolesAreas = this.nodesSelection.filter((node: any) => node.data.hasOwnProperty('roles') &&
      node.data.roles.length > 0)
      .append('g')
      .classed('roles-area', true)
      .attr('dx', (d: any) => d.x)
      .attr('dy', (d: any) => d.y + 40);

    rolesAreas.append('text')
      .text('-')
      .attr('dx', (d: any) => d.x)
      .attr('dy', (d: any) => d.y + 40)
      .style('text-anchor', 'middle');
    this.appendRolesLIst(rolesAreas);
    this.appendShowAll();
  }

  /**
   * Adds list of 6 first roles to filtered groups
   * @param rolesAreas - groups with at least one directly assigned role
   */
  private appendRolesLIst(rolesAreas: d3.Selection<SVGElement, any, any, any>) {
    rolesAreas.append('g')
      .classed('roles-list', true)
      .attr('dx', (d: any) => d.x)
      .attr('dy', (d: any) => d.y)
      .selectAll('.role')
      .data((d: any) => d.data.roles.slice(0, 6).map((role: any, index: number) => {
        return {id: role.id, uid: role.uid, x: d.x, y: d.y + index * 18}
      }))
      .enter()
        .append('text')
        .classed('role', true)
        .text((d: any) => d.uid)
        .style('text-anchor', 'middle')
        .attr('dx', (d: any) => d.x)
        .attr('dy', (d: any) => d.y + 56)
        .attr('data-id', (d: any) => d.id)
        .on('click', (d) => this.router.navigate(['role', d.target.dataset.id]));
  }

  /**
   * Adds show all button opening a modal window to groups with more than 6 directly assigned roles
   */
  private appendShowAll() {
    const longRolesAreas = this.nodesSelection.filter((node: any) => node.data.hasOwnProperty('roles') &&
      node.data.roles.length > 6).select('.roles-list');

    longRolesAreas.append('text')
      .text('Show all')
      .style('text-anchor', 'middle')
      .classed('show-all', true)
      .attr('dx', (d: any) => d.x)
      .attr('dy', (d: any) => d.y + 6*18 + 63)
      .attr('data-id', (d: any) => d.data.id)
      .attr('data-uid', (d: any) => d.data.uid)
      .on('click', this.openModal.bind(this))
  }

  /**
   * Removes all roles areas
   */
  private removeRolesInfo() {
    d3.selectAll('.roles-area').remove();
  }

  /**
   * Enables zoom and pan behavior
   */
  private enableZoom() {
    d3.select(this.canvas).call(d3.zoom().on('zoom', this.zoomed));
  }

  /**
   * Moves the tree by set coordinates
   * @param xCoordinate
   * @param yCoordinate
   */
  private transformTree(xCoordinate, yCoordinate) {
    d3.select(this.canvas)
      .call(d3.zoom().on('zoom', this.zoomed).transform,
      d3.zoomIdentity.translate(xCoordinate, yCoordinate));
  }

  /**
   * Moves the tree with animated transition by set coordinates
   * @param xCoordinate
   * @param yCoordinate
   */
  private transformTreeWithTransition(xCoordinate, yCoordinate) {
    d3.select(this.canvas)
      .transition()
      .duration(500)
      .call(d3.zoom().on('zoom', this.zoomed).transform,
        d3.zoomIdentity.translate(xCoordinate, yCoordinate))
      .transition();
  }

  /**
   * Calculates distance of the given element to center of the canvas
   * @param elementToMove - selected element from which the distance will be calculated
   */
  private calculateDistanceToCenter(elementToMove): [number, number] {
    const canvasSize = this.canvas.getBoundingClientRect();
    const elementDimensions = elementToMove.getBBox();
    const distanceToCenterX = (canvasSize.width / 2) - elementDimensions.x - (elementDimensions.width / 2);
    const distanceToCenterY = (canvasSize.height / 2) - elementDimensions.y - (elementDimensions.height / 2);
    return [distanceToCenterX, distanceToCenterY];
  }

  /**
   * Moves the root group to the center of the canvas
   */
  private moveRootToCenter() {
    const rootNode = this.treeSelection.select('#group-' + this.hierarchyRoot.data.id).node();
    this.transformTree(this.calculateDistanceToCenter(rootNode)[0], 20);
  }

  /**
   * Adds list of directly assigned roles to each group node
   * @param callback - callback function
   */
  private addRolesToGroup(callback = () => {}) {
    this.groupService.getRolesForDescendantGroups(this.rootGroup.id).subscribe(rolesMap => {
      this.hierarchyRoot.descendants().forEach(node => {
        node.data.roles = rolesMap[node.data.id];
      });
      this.rolesLoaded = true;
      this.rolesMap = rolesMap;
      callback();
    })
  }

  /**
   * Removes fullscreen class and focuses root group on fullscreen exit
   */
  private detectFullscreenExit() {
    if (!document.fullscreenElement) {
      this.canvas.classList.remove('fullscreen');
      this.focusGroup(this.currentGroupId, true);
    }
  }

  /**
   * Opens modal with all directly assigned roles for given group. Should be used only for groups with
   * too many roles
   * @param data
   */
  private openModal(data) {
    this.rolesModalGroupId = data.target.dataset.id;
    this.rolesModalUid = data.target.dataset.uid;
    this.cdr.detectChanges();
  }

  /**
   * Closes modal window
   */
  closeModal() {
    this.rolesModalGroupId = null;
  }
}
