import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from "@angular/platform-browser";
import { EntityHeaderComponent } from './entity/entity-header/entity-header.component';
import { ViewMenuComponent } from './entity/view-menu/view-menu.component';
import { AppRoutingModule } from "../app-routing.module";
import { RoleInformationComponent } from './entity/tabs/role-information/role-information.component';
import { UserInformationComponent } from './entity/tabs/user-information/user-information.component';
import { GroupInformationComponent } from './entity/tabs/group-information/group-information.component';
import { ScrollingModule } from "@angular/cdk/scrolling";
import { AssignmentIconComponent } from './tables/assignment-icon/assignment-icon.component';
import {FormsModule} from "@angular/forms";
import { GroupsTableComponent } from './tables/groups-table/groups-table.component';
import { UsersTableComponent } from './tables/users-table/users-table.component';
import { RolesTableComponent } from './tables/roles-table/roles-table.component';
import { GroupsMiniTableComponent } from './tables/groups-mini-table/groups-mini-table.component';
import { TableDescriptionComponent } from './tables/table-description/table-description.component';
import { TableComponent } from './tables/table/table.component';
import { GroupVisualizationComponent } from './entity/tabs/group-visualization/group-visualization.component';
import { GroupsTreeComponent } from './visualizations/groups-tree/groups-tree.component';



@NgModule({
    declarations: [EntityHeaderComponent,
      ViewMenuComponent,
      RoleInformationComponent,
      UserInformationComponent,
      GroupInformationComponent,
      AssignmentIconComponent,
      GroupsTableComponent,
      UsersTableComponent,
      RolesTableComponent,
      GroupsMiniTableComponent,
      TableDescriptionComponent,
      TableComponent,
      GroupVisualizationComponent,
      GroupsTreeComponent],
    exports: [
        EntityHeaderComponent,
        ViewMenuComponent,
        RolesTableComponent,
        TableDescriptionComponent,
        GroupsTableComponent,
        UsersTableComponent
    ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    ScrollingModule,
    FormsModule
  ]
})

export class ElementsModule { }
