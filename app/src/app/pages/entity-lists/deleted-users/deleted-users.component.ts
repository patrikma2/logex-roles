import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/resources/user.service";
import {User} from "../../../models/user.model";

@Component({
  selector: 'logex-deleted-users',
  templateUrl: './deleted-users.component.html',
  styleUrls: ['./deleted-users.component.scss']
})
export class DeletedUsersComponent implements OnInit {

  deletedUsers: User[] = null;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.loadDeletedUsers();
  }

  loadDeletedUsers() {
    this.userService.getDeleted().subscribe(users => {
      this.deletedUsers = users;
    })
  }

}
