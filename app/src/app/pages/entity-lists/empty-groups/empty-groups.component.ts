import { Component, OnInit } from '@angular/core';
import {Group} from "../../../models/group.model";
import {GroupService} from "../../../services/resources/group.service";

@Component({
  selector: 'logex-empty-groups',
  templateUrl: './empty-groups.component.html',
  styleUrls: ['./empty-groups.component.scss']
})
export class EmptyGroupsComponent implements OnInit {

  emptyGroups: Group[] = null;

  constructor(private groupService: GroupService) { }

  ngOnInit(): void {
    this.loadEmptyGroups();
  }

  loadEmptyGroups() {
    this.groupService.getEmpty().subscribe(groups => {
      this.emptyGroups = groups;
    })
  }
}
