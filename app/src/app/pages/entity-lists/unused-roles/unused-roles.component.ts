import { Component, OnInit } from '@angular/core';
import {RoleService} from "../../../services/resources/role.service";
import {Role} from "../../../models/role.model";

@Component({
  selector: 'logex-unused-roles',
  templateUrl: './unused-roles.component.html',
  styleUrls: ['./unused-roles.component.scss']
})
export class UnusedRolesComponent implements OnInit {

  unusedRoles: Role[] = null;

  constructor(private roleService: RoleService) { }

  ngOnInit(): void {
    this.loadUnusedRoles();
  }

  loadUnusedRoles() {
    this.roleService.getUnused().subscribe(roles => {
      this.unusedRoles = roles;
    })
  }

}
