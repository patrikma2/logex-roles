import { Component, OnInit } from '@angular/core';
import {Role} from "../../../models/role.model";
import {RoleService} from "../../../services/resources/role.service";

@Component({
  selector: 'logex-disabled-roles',
  templateUrl: './disabled-roles.component.html',
  styleUrls: ['./disabled-roles.component.scss']
})
export class DisabledRolesComponent implements OnInit {

  disabledRoles: Role[] = null;

  constructor(private roleService: RoleService) { }

  ngOnInit(): void {
    this.loadDisabledRoles();
  }

  loadDisabledRoles() {
    this.roleService.getDisabled().subscribe(roles => {
      this.disabledRoles = roles;
    })
  }

}
