import {Component, Input, OnInit} from '@angular/core';
import {Group} from "../../../models/group.model";
import {GroupService} from "../../../services/resources/group.service";

@Component({
  selector: 'logex-disabled-groups',
  templateUrl: './disabled-groups.component.html',
  styleUrls: ['./disabled-groups.component.scss']
})
export class DisabledGroupsComponent implements OnInit {

  @Input() disabledGroups: Group[];

  constructor(private groupService: GroupService) { }

  ngOnInit(): void {
    this.loadDisabledGroups();
  }

  loadDisabledGroups() {
    this.groupService.getDisabled().subscribe(groups => {
      this.disabledGroups = groups;
    })
  }
}
