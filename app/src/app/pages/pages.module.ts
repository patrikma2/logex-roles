import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {AppRoutingModule} from "../app-routing.module";
import {RouterModule} from "@angular/router";
import { UserViewComponent } from './entities/user-view/user-view.component';
import { RoleViewComponent } from './entities/role-view/role-view.component';
import {ElementsModule} from "../elements/elements.module";
import { GroupViewComponent } from './entities/group-view/group-view.component';
import { DisabledRolesComponent } from './entity-lists/disabled-roles/disabled-roles.component';
import { UnusedRolesComponent } from './entity-lists/unused-roles/unused-roles.component';
import { DisabledGroupsComponent } from './entity-lists/disabled-groups/disabled-groups.component';
import { EmptyGroupsComponent } from './entity-lists/empty-groups/empty-groups.component';
import { DeletedUsersComponent } from './entity-lists/deleted-users/deleted-users.component';
import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
  declarations: [HomeComponent, UserViewComponent, RoleViewComponent, GroupViewComponent, DisabledRolesComponent, UnusedRolesComponent, DisabledGroupsComponent, EmptyGroupsComponent, DeletedUsersComponent, NotFoundComponent],
    imports: [
        CommonModule,
        AppRoutingModule,
        ElementsModule
    ]
})

export class PagesModule { }
