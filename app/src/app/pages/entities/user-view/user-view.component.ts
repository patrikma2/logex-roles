import { Component, OnInit } from '@angular/core';
import {User} from "../../../models/user.model";
import {ActivatedRoute} from "@angular/router";
import {RoleService} from "../../../services/resources/role.service";
import {LocalStorageService} from "../../../services/local-storage.service";
import {UserService} from "../../../services/resources/user.service";
import {Tab} from "../../../models/tab.model";
import {OutletTransferService} from "../../../services/outlet-transfer.service";

@Component({
  selector: 'logex-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  user: User = null;
  loaded: boolean = false;
  tabs: Tab[] = [{text: 'Information', link: './'}];

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private localStorageService: LocalStorageService,
              private outletTransferService: OutletTransferService<User>) { }

  ngOnInit(): void {
    this.loadCurrentUser();
  }

  /**
   * Checks for new params from url, loads the user from API and saves it to local storage
   */
  loadCurrentUser() {
    this.route.paramMap.subscribe(params => {
      this.loaded = false;
      this.userService.getByIdWithAllRelations(parseInt(params.get('id'))).subscribe(user => {
        this.user = user;
        this.outletTransferService.send(user);
        this.loaded = true;
        this.saveVisitToLocalStorage();
      })
    });
  }

  /**
   * Saves visit to the local storage
   */
  saveVisitToLocalStorage() {
    this.localStorageService.saveVisitedItem('user',
      this.user.first_name + " " + this.user.last_name, this.user.user_id);
  }

  /**
   * Sends the user to router outlet
   * @param tab
   */
  onTabLoaded(tab) {
    tab.user = this.user;
  }
}
