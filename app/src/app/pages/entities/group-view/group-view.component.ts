import { Component, OnInit } from '@angular/core';
import {Role} from "../../../models/role.model";
import {Tab} from "../../../models/tab.model";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleService} from "../../../services/resources/role.service";
import {LocalStorageService} from "../../../services/local-storage.service";
import {Group} from "../../../models/group.model";
import {GroupService} from "../../../services/resources/group.service";
import {OutletTransferService} from "../../../services/outlet-transfer.service";

@Component({
  selector: 'logex-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.scss']
})
export class GroupViewComponent implements OnInit {

  loaded: boolean = false;
  group: Group = null;
  tabs: Tab[] = [
    {text: 'Information', link: './'},
    {text: 'Visualization', link: 'visualization'}
  ];

  constructor(private route: ActivatedRoute,
              private groupService: GroupService,
              private localStorageService: LocalStorageService,
              private outletTransferService: OutletTransferService<Group>) {}

  ngOnInit(): void {
    this.loadCurrentGroup();
  }

  /**
   * Checks for new params from url, loads the group from API and saves it to local storage
   */
  loadCurrentGroup() {
    this.route.paramMap.subscribe(params => {
      this.loaded = false;
      this.groupService.getByIdWithAllRelations(parseInt(params.get('id'))).subscribe(group => {
        this.group = group;
        this.outletTransferService.send(group);
        this.loaded = true;
        this.saveVisitToLocalStorage();
      })
    });
  }

  /**
   * Saves visit to the local storage
   */
  saveVisitToLocalStorage() {
    this.localStorageService.saveVisitedItem('group', this.group.uid, this.group.id);
  }

  /**
   * Sends the group to router outlet
   * @param tab
   */
  onTabLoaded(tab) {
    tab.group = this.group;
  }
}
