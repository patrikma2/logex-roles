import { Component, OnInit } from '@angular/core';
import {RoleService} from "../../../services/resources/role.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Role} from "../../../models/role.model";
import {LocalStorageService} from "../../../services/local-storage.service";
import {Tab} from "../../../models/tab.model";
import {OutletTransferService} from "../../../services/outlet-transfer.service";

@Component({
  selector: 'logex-role-view',
  templateUrl: './role-view.component.html',
  styleUrls: ['./role-view.component.scss']
})
export class RoleViewComponent implements OnInit {

  loaded: boolean = false;
  role: Role = null;
  tabs: Tab[] = [{text: 'Information', link: './'}];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private roleService: RoleService,
              private localStorageService: LocalStorageService,
              private outletTransferService: OutletTransferService<Role>) {}

  ngOnInit(): void {
    this.loadCurrentRole();
  }

  /**
   * Checks for new params from url, loads the role from API and saves it to local storage
   */
  loadCurrentRole() {
    this.route.paramMap.subscribe(params => {
      this.loaded = false;
      this.roleService.getByIdWithAllRelations(parseInt(params.get('id'))).subscribe(role => {
        this.role = role;
        this.outletTransferService.send(role)
        this.loaded = true;
        this.saveVisitToLocalStorage();
      })
    });
  }

  /**
   * Saves visit to the local storage
   */
  saveVisitToLocalStorage() {
    this.localStorageService.saveVisitedItem('role', this.role.uid, this.role.id);
  }

  /**
   * Sends the role to router outlet
   * @param tab
   */
  onTabLoaded(tab) {
    tab.role = this.role;
  }
}
