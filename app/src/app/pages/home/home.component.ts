import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from "../../services/local-storage.service";

@Component({
  selector: 'page-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  visitedItems: {type: string, name: string, id: number}[] = null;

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit(): void {
    this.visitedItems = this.localStorageService.getVisitedItems();
  }

}
