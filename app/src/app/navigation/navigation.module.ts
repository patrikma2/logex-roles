import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SidebarComponent} from "./sidebar/sidebar.component";
import {NavbarComponent} from "./navbar/navbar.component";
import {FormsModule} from "@angular/forms";
import {AppRoutingModule} from "../app-routing.module";
import {SidebarService} from "../services/sidebar.service";
import { SidebarSearchResultsComponent } from './sidebar-search-results/sidebar-search-results.component';
import { SidebarSearchResultsBlockComponent } from './sidebar-search-results-block/sidebar-search-results-block.component';
import {GroupService} from "../services/resources/group.service";
import {RoleService} from "../services/resources/role.service";
import {UserService} from "../services/resources/user.service";

@NgModule({
  declarations: [
    SidebarComponent,
    NavbarComponent,
    SidebarSearchResultsComponent,
    SidebarSearchResultsBlockComponent,
  ],
  exports: [
    SidebarComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [SidebarService,
    {
      provide: 'ResourceService', useClass: UserService, multi: true
    },
    {
      provide: 'ResourceService', useClass: RoleService, multi: true
    },
    {
      provide: 'ResourceService', useClass: GroupService, multi: true
    }]
})
export class NavigationModule { }
