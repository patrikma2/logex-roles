import {Component, Input, OnInit} from '@angular/core';
import {SidebarService} from "../../services/sidebar.service";

@Component({
  selector: 'logex-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() sidebarOpen: boolean;
  searchedText: string = '';

  constructor(private sidebarService: SidebarService) { }

  ngOnInit(): void {
  }

  /**
   * Checks if the sidebar is open
   */
  isOpen(): boolean {
    return this.sidebarService.isOpen();
  }

}
