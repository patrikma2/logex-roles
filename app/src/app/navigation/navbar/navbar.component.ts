import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SidebarService} from "../../services/sidebar.service";

@Component({
  selector: 'logex-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  menuOpen: boolean = false;
  @Output() sidebarOpen = new EventEmitter<boolean>();

  constructor(private sidebarService: SidebarService) { }

  ngOnInit(): void {
    this.sidebarOpen.emit(false);
  }

  /**
   * Opens or closes the menu for mobile version
   */
  handleMenuClick() {
    if (this.sidebarService.isOpen()) {
      this.sidebarService.closeSidebar();
    }
    this.menuOpen = !this.menuOpen;
  }

  /**
   * Only closes the menu
   */
  closeMenu() {
    if(this.menuOpen) {
      this.menuOpen = false;
    }
  }

  /**
   * Opens or closes the sidebar and closes the menu if necessary
   */
  handleSidebarClick() {
    if (this.menuOpen) {
      this.closeMenu();
    }
    this.sidebarService.switchSidebar();
  }
}
