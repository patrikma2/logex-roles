import {Component, Inject, Input, OnChanges, OnInit, SimpleChanges, TemplateRef} from '@angular/core';
import {ResourceService} from "../../services/resources/resource.service.interface";
import {Subscription} from "rxjs";

@Component({
  selector: 'sidebar-search-results-block',
  templateUrl: './sidebar-search-results-block.component.html',
  styleUrls: ['./sidebar-search-results-block.component.scss']
})
export class SidebarSearchResultsBlockComponent implements OnInit, OnChanges {
  private substring: string = '';
  private service: number = null;
  private maxResultsOnPage = 5;

  @Input('heading') heading: string;
  @Input('searchedText') searchedText: string;
  @Input('serviceId') serviceId: number;
  @Input('template') template: TemplateRef<any>;

  loadedResults: any[] = [];
  nextResultsLoading: boolean = false;
  moreResultsAvailable: boolean = false;
  firstResultsObserver: Subscription = null;

  currentPage: number = 0;
  lastPage: number = null;

  constructor(@Inject('ResourceService')private resourceService: ResourceService[]) {}

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.searchedText) {
      this.substring = changes.searchedText.currentValue;
    }
    if (changes.serviceId) {
      this.service = changes.serviceId.currentValue;
    }
    if (this.service !== null) {
      this.reset();
      this.loadFirstResults();
    }
  }

  /**
   * Resets the state
   */
  reset() {
    this.loadedResults = [];
    this.nextResultsLoading = false;
    this.moreResultsAvailable = false;
    this.currentPage = 0;
    this.lastPage = null;
    if (this.firstResultsObserver !== null) {
      this.firstResultsObserver.unsubscribe();
      this.firstResultsObserver = null;
    }
  }

  /**
   * Gets current part of array with loaded results
   */
  get resultsOnCurrentPage(): any[] {
    return this.loadedResults.slice(this.currentPage * this.maxResultsOnPage,
      this.currentPage * this.maxResultsOnPage + this.maxResultsOnPage)
  }

  /**
   * Checks if last page is shown
   */
  get isOnLastPage(): boolean {
    return this.currentPage === this.lastPage;
  }

  /**
   * Checks if navigation arrows should be displayed
   */
  get shouldDisplayArrows(): boolean {
    return (this.loadedResults.length === this.maxResultsOnPage && this.moreResultsAvailable) ||
      (this.loadedResults.length > this.maxResultsOnPage);
  }

  /**
   * Decides what to do next on next arrow click
   */
  handleNextClick() {
    if (this.moreResultsAvailable && this.isOnLastPage) {
      this.loadNextResults();
      return;
    }
    if ((this.currentPage !== this.lastPage)) {
      this.currentPage++;
    }
  }

  /**
   * Decides what to do next on previous arrow click
   */
  handlePreviousClick() {
    if (this.currentPage === 0) {
      return;
    }
    this.currentPage--;
  }

  /**
   * Loads 5 first results and marks if at least one more result is available
   */
  loadFirstResults() {
    this.firstResultsObserver = this.resourceService[this.service]
      .get(null, this.maxResultsOnPage + 1, this.substring).subscribe(newEntities => {
      if (newEntities.length === (this.maxResultsOnPage + 1)) {
        this.moreResultsAvailable = true;
        newEntities.pop();
      } else {
        this.moreResultsAvailable = false;
      }
      this.lastPage = 0;
      this.loadedResults = newEntities;
    });
  }

  /**
   * Loads 5 more results
   */
  loadNextResults() {
    this.nextResultsLoading = true;
    this.resourceService[this.service].get(this.loadedResults.length, this.maxResultsOnPage + 1, this.substring)
      .subscribe(
      newEntities => {
      if (newEntities.length === (this.maxResultsOnPage + 1)) {
        this.moreResultsAvailable = true;
        newEntities.pop();
      } else {
        this.moreResultsAvailable = false;
      }
      this.loadedResults = this.loadedResults.concat(newEntities);
      this.currentPage++;
      this.lastPage = this.currentPage;
      this.nextResultsLoading = false;
    });
  }
}
