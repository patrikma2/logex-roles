import {Component, Input, OnInit} from '@angular/core';
import {SidebarService} from "../../services/sidebar.service";

@Component({
  selector: 'sidebar-search-results',
  templateUrl: './sidebar-search-results.component.html',
  styleUrls: ['./sidebar-search-results.component.scss']
})
export class SidebarSearchResultsComponent implements OnInit {

  @Input('searchedText') searchedText: string;

  constructor(private sidebarService: SidebarService) { }

  ngOnInit(): void { }

  /**
   * Closes the sidebar on smaller screens
   */
  closeSidebar() {
    if (window.innerWidth < 1620) {
      this.sidebarService.closeSidebar();
    }
  }
}
