// Load .env file
require('dotenv').config();

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sequelize = require('./config/database');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/api/users');
const rolesRouter = require('./routes/api/roles');
const groupsRouter = require('./routes/api/groups');
const productsRouter = require('./routes/api/products');
const registerAssociations = require('./models/utilities/associations');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'public'));

// Register associations between models
registerAssociations();

// Register routers
app.use('/api', usersRouter);
app.use('/api', rolesRouter);
app.use('/api', groupsRouter);
app.use('/api', productsRouter);
app.use('/', indexRouter);

// Check connection
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(e => {
    console.error('Unable to connect to the database:', e);
  });

module.exports = app;
