const express = require('express');
const router = express.Router();
const {getGroups,
  getGroupsWithOffsetLimit,
  getGroupsBySubstring,
  getGroupsBySubstringWithOffsetLimit,
  getGroup,
  getGroupWithAllRelations,
  getDisabledGroups,
  getEmptyGroups,
  getDirectRolesForDescendantGroups} = require('./../../controllers/groups');

/**
 * List of all groups
 */
router.get('/groups', getGroups);

/**
 * List of several first groups with the given offset. Setting limit or offset to null disables it.
 */
router.get('/groups/offset/:offset/limit/:count', getGroupsWithOffsetLimit);

/**
 * List of groups with uid containing the given substring (for searching)
 */
router.get('/groups/substring/:substring', getGroupsBySubstring);

/**
 * List of several first groups with uid containing the given substring (for searching), with the given offset.
 * Setting limit or offset to null disables it.
 */
router.get('/groups/substring/:substring/offset/:offset/limit/:count', getGroupsBySubstringWithOffsetLimit);

/**
 * List of groups with 'disabled' flag
 */
router.get('/groups/disabled', getDisabledGroups);

/**
 * List of groups without directly assigned roles and without inherited roles
 */
router.get('/groups/empty', getEmptyGroups);

/**
 * Group with the given ID
 */
router.get('/group/:groupId', getGroup);

/**
 * Group with the given ID and all associations, including product, ancestor/descendant groups
 * and roles/users associated with them
 */
router.get('/group/:groupId/all-relations', getGroupWithAllRelations);

/**
 * Object with keys representing group ids of descendent groups and values with lists of their directly assigned groups
 */
router.get('/group/:groupId/roles-for-descendants', getDirectRolesForDescendantGroups);

module.exports = router;
