const express = require('express');
const router = express.Router();
const {getUsers,
  getUsersWithOffsetLimit,
  getUsersBySubstring,
  getUsersBySubstringWithOffsetLimit,
  getUser,
  getUserWithAllRelations,
  getDeletedUsers} = require('./../../controllers/users');

/*
 * List of all users
 */
router.get('/users', getUsers);

/**
 * List of several first users with the given offset. Setting limit or offset to null disables it.
 */
router.get('/users/offset/:offset/limit/:count', getUsersWithOffsetLimit);

/**
 * List of users whose name contains the given substring (for searching)
 */
router.get('/users/substring/:substring', getUsersBySubstring);

/**
 * List of several first users with name containing the given substring (for searching),
 * with the given offset. Setting limit or offset to null disables it.
 */
router.get('/users/substring/:substring/offset/:offset/limit/:count', getUsersBySubstringWithOffsetLimit);

/**
 * List of users with flag 'is_deleted'
 */
router.get('/users/deleted', getDeletedUsers);

/**
 * User with given ID
 */
router.get('/user/:userId', getUser);

/**
 * User with the given ID and all associations, including inherited groups and roles the user is in
 */
router.get('/user/:userId/all-relations', getUserWithAllRelations);

module.exports = router;
