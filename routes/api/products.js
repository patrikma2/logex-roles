const express = require('express');
const router = express.Router();

const {getProducts,
  getProduct} = require('./../../controllers/products');


/**
 * List of all products
 */
router.get('/products', getProducts);

/**
 * Product with the given ID
 */
router.get('/product/:productId', getProduct);

module.exports = router;