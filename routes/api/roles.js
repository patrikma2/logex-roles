const express = require('express');
const router = express.Router();
const {getRoles,
  getRolesWithOffsetLimit,
  getRolesBySubstring,
  getRolesBySubstringWithOffsetLimit,
  getRole,
  getRoleWithAllRelations,
  getDisabledRoles,
  getUnusedRoles} = require('./../../controllers/roles');

/**
 * List of all roles
 */
router.get('/roles', getRoles);

/**
 * List of several first roles with the given offset. Setting limit or offset to null disables it.
 */
router.get('/roles/offset/:offset/limit/:count', getRolesWithOffsetLimit);

/**
 * List of roles with uid containing the given substring (for searching)
 */
router.get('/roles/substring/:substring', getRolesBySubstring);

/**
 * List of several first roles with uid containing the given substring (for searching), with the given offset.
 * Setting limit or offset to null disables it.
 */
router.get('/roles/substring/:substring/offset/:offset/limit/:count', getRolesBySubstringWithOffsetLimit);

/**
 * List of roles with 'disabled' flag
 */
router.get('/roles/disabled', getDisabledRoles);

/**
 * List of roles not assigned to any users and groups
 */
router.get('/roles/unused', getUnusedRoles);

/**
 * Role with the given ID
 */
router.get('/role/:roleId', getRole);

/**
 * Role with the given ID and all associations, including inherited groups featuring this role and
 * users using this role
 */
router.get('/role/:roleId/all-relations', getRoleWithAllRelations);

module.exports = router;
