const {Op, QueryTypes, where, col} = require('sequelize');
const sequelize = require('../config/database');

const Role = require('./../models/Role');
const User = require('./../models/User');
const Group = require('./../models/Group');
const Product = require('./../models/Product');

/**
 * Gets a list of all groups
 */
exports.getGroups = (req, res) => {
  Group.findAll({
    order: [['id', 'ASC']]
  }).then(groups => res.json(groups));
};


/**
 * Gets a list of several first groups with the given offset. Setting limit or offset to null disables it.
 */
exports.getGroupsWithOffsetLimit = (req, res) => {
  Group.findAll({
    offset: parseInt(req.params.offset),
    limit: parseInt(req.params.count),
    order: [['id', 'ASC']]
  }).then(groups => res.json(groups))
    .catch(e => {
      console.error(e);
      res.json([]);
    });
};


/**
 * Gets a list of groups with uid containing the given substring (for searching)
 */
exports.getGroupsBySubstring = (req, res) => {
  const searchedGroup = req.params.substring.replace(/_/g, '[_]');
  Group.findAll({
    order: [['id', 'ASC']],
    where: {
      uid: {
        [Op.substring]: searchedGroup
      }
    }
  }).then(groups => res.json(groups));
};

/**
 * Gets a list of several first groups with uid containing the given substring (for searching), with the given offset.
 * Setting limit or offset to null disables it.
 */
exports.getGroupsBySubstringWithOffsetLimit = (req, res) => {
  const searchedGroup = req.params.substring.replace(/_/g, '[_]');
  Group.findAll({
    offset: parseInt(req.params.offset),
    limit: parseInt(req.params.count),
    order: [['id', 'ASC']],
    where: {
      uid: {
        [Op.substring]: searchedGroup
      }
    }
  }).then(groups => res.json(groups));
};

/**
 * Gets a list of groups with 'disabled' flag
 */
exports.getDisabledGroups = (req, res) => {
  Group.findAll({
    where: {
      is_disabled: true
    }
  }).then(groups => res.json(groups));
};


/**
 * Gets a list of groups without directly assigned roles and without inherited roles
 */
exports.getEmptyGroups = async (req, res) => {
  const groupsWithoutRoles = await Group.findAll({
    include: [
      {model: Role, as: 'roles_direct', attributes: ['id'], required: false}],
    where: [
      where(col('roles_direct.id'), null)]
  });
  let groupsWithoutInheritedRoles = [];
  for (const group of groupsWithoutRoles) {
    const ancestorsWithRoles = await hasAncestorsWithRoles(group.id);
    if (ancestorsWithRoles.length === 0) {
      groupsWithoutInheritedRoles.push(group);
    }
  }
  res.json(groupsWithoutInheritedRoles);
};

/**
 * Gets a single group with the given ID
 */
exports.getGroup = (req, res) => {
  Group.findByPk(req.params.groupId).then(group => res.json(group));
};

/**
 * Gets a group with the given ID and all associations, including product, parent/child groups
 * and roles/users associated with them
 */
exports.getGroupWithAllRelations = async (req, res) => {
  try {
    const groupId = req.params.groupId;
    const group = await Group.findByPk(groupId, {
      include: [
        {model: Product, as: 'product'},
        {model: Role, as: 'roles_direct', through: {attributes: []}},
        {model: User, as: 'users_direct', through: {attributes: []}}
      ]
    });
    group.setDataValue('descendant_groups', await findDescendantGroups(groupId));
    group.setDataValue('sibling_groups', await findSiblingGroups(groupId, group.parent_group_id));
    group.setDataValue('ancestor_groups', await findAncestorGroups(groupId));
    group.setDataValue('roles_inherited', await findRolesInAncestorGroups(groupId));
    group.setDataValue('users_inherited', await findUsersInDescendantGroups(groupId));
    res.json(group);
  } catch (e) {
    console.error(e);
    res.json(null);
  }
};

/**
 * Gets an object with keys representing group ids of descendent groups and values with lists of their
 * directly assigned groups
 */
exports.getDirectRolesForDescendantGroups = async (req, res) => {
  try {
    const childGroupsAndRoles = await findDescendantGroupsWithRoles(req.params.groupId);
    const groupIdsWithRolesInArray = addRolesToGroups(childGroupsAndRoles);
    res.json(groupIdsWithRolesInArray);
  } catch (e) {
    console.error(e);
    res.json({});
  }
};

/**
 * Creates an object (working as a map) with id of a group as a key and list of its direct roles as a value
 * It is used to transfer roles for each group via JSON
 * @param groupsWithRoles - result from findDescendantGroupsWithRoles
 * @return {{id: []}}
 */
const addRolesToGroups = (groupsWithRoles) => {
  let rolesForGroupId = {};
  groupsWithRoles.forEach(groupRole => {
    if (!rolesForGroupId.hasOwnProperty(groupRole.group_id)) {
      rolesForGroupId[groupRole.group_id] = [];
    }
    if (groupRole.role_id !== null) {
      const newRole = {
        id: groupRole.role_id,
        uid: groupRole.role_uid,
        role_description: groupRole.role_description,
        product_id: groupRole.product_id
      };
      rolesForGroupId[groupRole.group_id].push(newRole);
    }
  });
  return rolesForGroupId;
};

/**
 * Checks if a group inherits at least one role from any of its ancestors
 * @param groupId
 * @return Promise<[]> non-empty array if it does, empty array if it doesn´t
 */
const hasAncestorsWithRoles = (groupId) => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G WHERE G.id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.id = C.parent_group_id\n' +
    ')\n' +
    'SELECT TOP 1 1 AS found\n' +
    'FROM CTE LEFT JOIN auth.GroupRoles AS GR ON CTE.id = GR.group_id\n' +
    'WHERE GR.group_id IS NOT NULL\n' +
    'EXCEPT SELECT G.id FROM auth.Groups AS G WHERE G.id = ?', {
    replacements: [groupId, groupId],
    type: QueryTypes.SELECT
  })
};

/**
 * Gets a list of descendants for the group with the given ID
 * @param groupId
 * @return Promise<[]>
 */
const findDescendantGroups = groupId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G WHERE G.parent_group_id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.parent_group_id = C.id\n' +
    ')\n' +
    'SELECT *\n' +
    'FROM CTE\n', {
    replacements: [groupId],
    type: QueryTypes.SELECT
  })
};

/**
 * For each group it finds all directly assigned roles
 * @param groupId
 * @return Promise<[]>
 */
const findDescendantGroupsWithRoles = groupId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id , G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '  WHERE G.id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id , G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.parent_group_id = C.id\n' +
    ')\n' +
    'SELECT CTE.id AS group_id, CTE.uid AS group_uid, CTE.group_description, CTE.parent_group_id , CTE.product_id,\n' +
    'R.id AS role_id, R.uid AS role_uid, R.role_description\n' +
    'FROM CTE  \n' +
    'LEFT JOIN auth.GroupRoles AS GR ON CTE.id = GR.group_id\n' +
    'LEFT JOIN auth.Roles AS R ON GR.role_id = R.id\n' +
    'ORDER BY CTE.id', {
    replacements: [groupId],
    type: QueryTypes.SELECT
  })
};

/**
 * Gets groups with the same parent, except for: those with null parent and the group with the groupId
 * @param groupId
 * @param groupParentId
 * @return {Promise<Group[]>}
 */
const findSiblingGroups = (groupId, groupParentId) => {
  return Group.findAll({
    where: {
      [Op.and]: [
        {
          parent_group_id: groupParentId
        },
        {
          parent_group_id: {[Op.ne]: null}
        },
        {
          id: {[Op.ne]: groupId}
        }
      ]
    }
  })
};

/**
 * Gets all ancestors
 * @param groupId
 * @return {Promise<[]>}
 */
const findAncestorGroups = groupId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G WHERE G.id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.id = C.parent_group_id\n' +
    ')\n' +
    'SELECT *\n' +
    'FROM CTE\n' +
    'EXCEPT SELECT TOP 1 * FROM CTE', {
    replacements: [groupId],
    type: QueryTypes.SELECT
  })
};

/**
 * Gets inherited roles
 * @param groupId
 * @return {Promise<[]>}
 */
const findRolesInAncestorGroups = groupId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G WHERE G.id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.id = C.parent_group_id\n' +
    ')\n' +
    'SELECT DISTINCT R.id, R.uid, R.role_description\n' +
    'FROM CTE\n' +
    'JOIN auth.GroupRoles AS GR ON GR.group_id = CTE.id\n' +
    'JOIN auth.Roles AS R ON GR.role_id = R.id\n' +
    'WHERE CTE.id != ?\n' +
    'ORDER BY R.id', {
    replacements: [groupId, groupId],
    type: QueryTypes.SELECT
  })
};

/**
 * Gets users in descendant groups
 * @param groupId
 * @return {Promise<[]>}
 */
const findUsersInDescendantGroups = groupId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G WHERE G.parent_group_id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.parent_group_id = C.id\n' +
    ')\n' +
    'SELECT DISTINCT U.user_id, U.login, U.first_name, U.last_name\n' +
    'FROM CTE\n' +
    'JOIN auth.UserGroups AS UG ON UG.group_id = CTE.id\n' +
    'JOIN auth.Users AS U ON UG.user_id = U.user_id', {
    replacements: [groupId],
    type: QueryTypes.SELECT
  })
};
