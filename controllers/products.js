const Product = require('./../models/Product');

/**
 * Gets a list of all products
 */
exports.getProducts = (req, res) => {
  Product.findAll().then(products => res.json(products));
};

/**
 * Gets a single product with the given ID
 */
exports.getProduct = (req, res) => {
  Product.findByPk(req.params.productId).then(product => res.json(product));
};
