const {Op, QueryTypes} = require('sequelize');
const sequelize = require('../config/database');

const Role = require('./../models/Role');
const User = require('./../models/User');
const Group = require('./../models/Group');

/*
 * Gets a list of all users
 */
exports.getUsers = (req, res) => {
  User.findAll({
    order: [['user_id', 'ASC']]
  }).then(users => res.json(users));
};

/**
 * Gets a list of several first users with the given offset. Setting limit or offset to null disables it.
 */
exports.getUsersWithOffsetLimit = (req, res) => {
  User.findAll({
    offset: parseInt(req.params.offset),
    limit: parseInt(req.params.count),
    order: [['user_id', 'ASC']]
  }).then(users => res.json(users))
    .catch(e => {
      console.error(e);
      res.json([]);
    });
};

/**
 * Gets a list of users whose name or login contains the given substring (for searching)
 */
exports.getUsersBySubstring = (req, res) => {
  const [searchedText, possibleFirstName, possibleLastName] = getPossibleNames(req.params.substring);
  User.findAll({
    where: {
      [Op.or] : [
        {first_name: {[Op.substring]: searchedText}},
        {last_name: {[Op.substring]: searchedText}},
        {[Op.and] : [
            {first_name: {[Op.substring]: possibleFirstName}},
            {last_name: {[Op.substring]: possibleLastName}},
          ]}
      ]
    }
  }).then(users => res.json(users));
};


/**
 * Gets a list of several first users with name containing the given substring (for searching),
 * with the given offset. Setting limit or offset to null disables it.
 */
exports.getUsersBySubstringWithOffsetLimit = (req, res) => {
  const [searchedText, possibleFirstName, possibleLastName] = getPossibleNames(req.params.substring);
  User.findAll({
    offset: parseInt(req.params.offset),
    limit: parseInt(req.params.count),
    order: [['user_id', 'ASC']],
    where: {
      [Op.or] : [
        {first_name: {[Op.substring]: searchedText}},
        {last_name: {[Op.substring]: searchedText}},
        {[Op.and] : [
            {first_name: {[Op.substring]: possibleFirstName}},
            {last_name: {[Op.substring]: possibleLastName}},
          ]}
      ]
    }
  }).then(users => res.json(users))
    .catch(e => {
      console.error(e);
      res.json([]);
    });
};

/**
 * Gets a list of users with flag 'is_deleted'
 */
exports.getDeletedUsers = (req, res) => {
  User.findAll({
    where: {
      is_deleted: true
    }
  }).then(deletedUsers => {
    res.json(deletedUsers);
  })
};

/**
 * Gets a user with given ID
 */
exports.getUser = (req, res) => {
  User.findByPk(req.params.userId).then(user => res.json(user));
};

/**
 * Gets a user with the given ID and all associations, including inherited groups and roles the user is in
 */
exports.getUserWithAllRelations = async (req, res) => {
  try {
    const userId = req.params.userId;
    const user = await User.findByPk(userId, {
      include: [
        {model: Group, as: 'groups_direct', through: {attributes: []}},
        {model: Role, as: 'roles_direct', through: {attributes: []}}
      ]
    });
    user.setDataValue('roles_inherited', await findRolesAssignedThroughGroupsByUserId(userId));
    user.setDataValue('groups_inherited', await findAncestorGroupsByUserId(userId));
    res.json(user);
  } catch (e) {
    console.error(e);
    res.json(null);
  }
};

/**
 * Normalizes the searched text (č is changed to c etc.) and divides it into 3 parts:
 * first string: full searched text
 * second and third string: if the results contains two words, it divides them as first and last names, if
 * it does not, both are null
 * @param searchedText - text searched by user
 * @return {[string, string | null, string | null]}
 */
const getPossibleNames = (searchedText) => {
  let fullName = searchedText.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  fullName = fullName.replace(/_/g, '[_]');
  let possibleFirstName = null, possibleLastName = null;
  const separated = fullName.split(' ');
  if (separated.length === 2) {
    possibleFirstName = separated[0];
    possibleLastName = separated[1];
  }
  return [fullName, possibleFirstName, possibleLastName];
};

/**
 * Finds roles this user has acquired through groups and their ancestors
 * @param userId
 * @return {Promise<[]>}
 */
const findRolesAssignedThroughGroupsByUserId = userId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.group_description, G.parent_group_id \n' +
    '  FROM auth.Groups AS G\n' +
    '  JOIN auth.UserGroups AS UG ON G.id = UG.group_id\n' +
    '  JOIN auth.Users AS U ON U.user_id = UG.user_id WHERE U.user_id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.group_description, G.parent_group_id \n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN CTE AS C\n' +
    '      ON G.id = C.parent_group_id\n' +
    ')\n' +
    'SELECT DISTINCT R.id, R.uid, R.role_description, R.product_id\n' +
    'FROM CTE  \n' +
    'JOIN auth.GroupRoles AS GR ON CTE.id = GR.group_id\n' +
    'JOIN auth.Roles AS R ON GR.role_id = R.id\n' +
    'ORDER BY R.id', {
    replacements: [userId],
    type: QueryTypes.SELECT
  })
};

/**
 * Finds ancestors of groups the user is a member of
 * @param userId
 * @return {Promise<[]>}
 */
const findAncestorGroupsByUserId = userId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G \n' +
    '  JOIN auth.UserGroups AS UG ON UG.group_id = G.id\n' +
    '  JOIN auth.Users as U ON UG.user_id = U.user_id WHERE U.user_id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN cte AS C\n' +
    '      ON G.id = C.parent_group_id\n' +
    ')\n' +
    'SELECT DISTINCT *\n' +
    'FROM CTE\n' +
    'EXCEPT SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id\n' +
    '  FROM auth.Groups AS G \n' +
    '  JOIN auth.UserGroups AS UG ON UG.group_id = G.id\n' +
    '  JOIN auth.Users as U ON UG.user_id = U.user_id WHERE U.user_id = ?\n' +
    'ORDER BY id', {
    replacements: [userId, userId],
    type: QueryTypes.SELECT
  })
};
