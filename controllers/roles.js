const {Op, QueryTypes, where, col} = require('sequelize');
const sequelize = require('../config/database');

const Role = require('./../models/Role');
const User = require('./../models/User');
const Group = require('./../models/Group');
const Product = require('./../models/Product');

/**
 * Gets a list of all roles
 */
exports.getRoles = (req, res) => {
  Role.findAll().then(roles => res.json(roles));
};

/**
 * Gets a list of several first roles with the given offset. Setting limit or offset to null disables it.
 */
exports.getRolesWithOffsetLimit = (req, res) => {
  Role.findAll({
    offset: parseInt(req.params.offset),
    limit: parseInt(req.params.count),
    order: [['id', 'ASC']]
  }).then(roles => res.json(roles))
    .catch(e => {
      console.error(e);
      res.json([]);
    });
};

/**
 * Gets a list of roles with uid containing the given substring (for searching)
 */
exports.getRolesBySubstring = (req, res) => {
  const searchedRole = req.params.substring.replace(/_/g, '[_]');
  Role.findAll({
    where: {
      uid: {
        [Op.substring]: searchedRole
      }
    }
  }).then(roles => res.json(roles));
};

/**
 * Gets a list of several first roles with uid containing the given substring (for searching), with the given offset.
 * Setting limit or offset to null disables it.
 */
exports.getRolesBySubstringWithOffsetLimit = (req, res) => {
  const searchedRole = req.params.substring.replace(/_/g, '[_]');
  Role.findAll({
    offset: parseInt(req.params.offset),
    limit: parseInt(req.params.count),
    order: [['id', 'ASC']],
    where: {
      uid: {
        [Op.substring]: searchedRole
      }
    }
  }).then(roles => res.json(roles));
};

/**
 * Gets a list of roles with 'disabled' flag
 */
exports.getDisabledRoles = (req, res) => {
  Role.findAll({
    where: {
      is_disabled: true
    }
  }).then(roles => res.json(roles));
};

/**
 * Gets a list of roles not assigned to any users and groups
 */
exports.getUnusedRoles = (req, res) => {
   Role.findAll({
    include: [
      {model: Group, as: 'groups_direct', attributes: ['id'], required: false},
      {model: User, as: 'users_direct', attributes: ['user_id'], required: false}],
      where: [
        where(col('groups_direct.id'), null),
        where(col('users_direct.user_id'), null),
     ]
  }).then(unusedRoles => res.json(unusedRoles));
};

/**
 * Gets a single role with the given ID
 */
exports.getRole = (req, res) => {
  Role.findByPk(req.params.roleId).then(role => res.json(role)).catch(() => res.json(null));
};

/**
 * Gets a role with the given ID and all associations, including inherited groups featuring this role and
 * users using this role
 */
exports.getRoleWithAllRelations = async (req, res) => {
  try {
    const roleId = req.params.roleId;
    const role = await Role.findByPk(roleId, {
      include: [
        {model: Product, as: 'product'},
        {model: Group, as: 'groups_direct', through: {attributes: []}},
        {model: User, as: 'users_direct', through: {attributes: []}}
      ]
    });
    role.setDataValue('groups_inherited', await findDescendantGroupsByRoleId(roleId));
    role.setDataValue('users_through_groups', await findUsersAssignedThroughGroupsByRoleId(roleId));
    res.json(role);
  } catch (e) {
    console.error(e);
    res.json(null);
  }
};

/**
 * Finds descendant groups which contain role with the given ID
 * @param roleId
 * @return {Promise<[]>}
 */
const findDescendantGroupsByRoleId = roleId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id \n' +
    '  FROM auth.Groups AS G\n' +
    '  JOIN auth.GroupRoles AS GR ON G.id = GR.group_id\n' +
    '  JOIN auth.Roles AS R ON R.id = GR.role_id WHERE R.id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id \n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN CTE AS C\n' +
    '      ON G.parent_group_id = C.id\n' +
    ')\n' +
    'SELECT *\n' +
    'FROM CTE\n' +
    'EXCEPT SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id \n' +
    '  FROM auth.Groups AS G\n' +
    '  JOIN auth.GroupRoles AS GR ON G.id = GR.group_id\n' +
    '  JOIN auth.Roles AS R ON R.id = GR.role_id WHERE R.id = ?\n', {
    replacements: [roleId, roleId],
    type: QueryTypes.SELECT
  })
};

/**
 * Finds users with role with the given ID assigned through groups and their ancestors
 * @param roleId
 * @return {Promise<[]>}
 */
const findUsersAssignedThroughGroupsByRoleId = roleId => {
  return sequelize.query(';WITH CTE AS\n' +
    '(  \n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id \n' +
    '  FROM auth.Groups AS G\n' +
    '  JOIN auth.GroupRoles AS GR ON G.id = GR.group_id\n' +
    '  JOIN auth.Roles AS R ON R.id = GR.role_id WHERE R.id = ?\n' +
    '  UNION ALL\n' +
    '  SELECT G.id, G.uid, G.group_description, G.parent_group_id, G.product_id \n' +
    '  FROM auth.Groups AS G\n' +
    '    INNER JOIN CTE AS C\n' +
    '      ON G.parent_group_id = C.id\n' +
    ')\n' +
    'SELECT DISTINCT U.user_id, U.title, U.login, U.first_name, U.last_name, U.email\n' +
    'FROM CTE  \n' +
    'JOIN auth.UserGroups AS Ug ON CTE.id = UG.group_id\n' +
    'JOIN auth.Users AS U ON UG.user_id = U.user_id\n' +
    'ORDER BY u.user_id', {
    replacements: [roleId],
    type: QueryTypes.SELECT
  })
};
