const request = require("supertest");
const app = require("../../app");

describe("Test index page", () => {
  test("Status code", async () => {
    const response = await request(app).get("/");
    expect(response.statusCode).toBe(200);
  });
});