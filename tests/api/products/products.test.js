const request = require("supertest");
const app = require("../../../app");

const allProductsJson = require('./json/all-products');
const singleProductJson = require('./json/single-product-1');

const responses = {
  allProducts: [],
  selectedProduct: {}
};

beforeAll(async () => {
  responses.allProducts = await request(app).get("/api/products");
  responses.selectedProduct = await request(app).get("/api/product/1");
});

describe('Test all products', () => {
  test("Status code", () => {
    expect(responses.allProducts.statusCode).toBe(200);
  });
  test('Number of products', () => {
    expect(responses.allProducts.body.length).toBe(3);
  });
  test('All content', async () => {
    expect(responses.allProducts.body).toEqual(allProductsJson);
  });
});

describe('Test selected product (ID = 1)', () => {
  test("Status code", () => {
    expect(responses.selectedProduct.statusCode).toBe(200);
  });
  test('Product content', async () => {
    expect(responses.selectedProduct.body).toEqual(singleProductJson);
  });
});