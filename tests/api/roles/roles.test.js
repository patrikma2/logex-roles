const request = require("supertest");
const app = require("../../../app");

const allRolesJson = require('./json/all-roles');
const allRolesSubstringBgJson = require('./json/all-roles-substring-bg');
const id7Json = require('./json/single-role-7-all');

const responses = {
  allRoles: [],
  allRolesSubstring: [],
  selectedRole: {}
};

beforeAll(async () => {
  responses.allRoles = await request(app).get("/api/roles");
  responses.allRolesSubstring = await request(app).get("/api/roles/substring/bg");
  responses.selectedRole = await request(app).get('/api/role/7/all-relations')
});

describe('Test all roles', () => {
  test("Status code", () => {
    expect(responses.allRoles.statusCode).toBe(200);
  });
  test('Number of roles', () => {
    expect(responses.allRoles.body.length).toBe(16);
  });
  test('All content', async () => {
    expect(responses.allRoles.body).toEqual(allRolesJson);
  });
});

describe('Test all roles with substring bg', () => {
  test("Status code", () => {
    expect(responses.allRolesSubstring.statusCode).toBe(200);
  });
  test('Number of roles', () => {
    expect(responses.allRolesSubstring.body.length).toBe(5);
  });
  test('All content', async () => {
    expect(responses.allRolesSubstring.body).toEqual(allRolesSubstringBgJson);
  });
});

describe('Test selected role (ID = 7) with associations', () => {
  test("Status code", () => {
    expect(responses.selectedRole.statusCode).toBe(200);
  });
  test('Basic info', async () => {
    expect(responses.selectedRole.body.id).toBe(7);
    expect(responses.selectedRole.body.uid).toBe('fcm_edit');
  });
  test('All content', async () => {
    expect(responses.selectedRole.body).toEqual(id7Json);
  });
});
