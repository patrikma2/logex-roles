const request = require("supertest");
const app = require("../../../app");

const allGroupsJson = require('./json/all-groups');
const allGroupsSubstringBgJson = require('./json/all-groups-substring-bg');
const id19Json = require('./json/single-group-19-all');

const responses = {
  allGroups: [],
  allGroupsSubstring: [],
  selectedGroup: {}
};

beforeAll(async () => {
  responses.allGroups = await request(app).get("/api/groups");
  responses.allGroupsSubstring = await request(app).get("/api/groups/substring/bg");
  responses.selectedGroup = await request(app).get('/api/group/19/all-relations')
});

describe('Test all groups', () => {
  test("Status code", () => {
    expect(responses.allGroups.statusCode).toBe(200);
  });
  test('Number of groups', () => {
    expect(responses.allGroups.body.length).toBe(17);
  });
  test('All content', async () => {
    expect(responses.allGroups.body).toEqual(allGroupsJson);
  });
});

describe('Test all groups with substring bg', () => {
  test("Status code", () => {
    expect(responses.allGroupsSubstring.statusCode).toBe(200);
  });
  test('Number of groups', () => {
    expect(responses.allGroupsSubstring.body.length).toBe(7);
  });
  test('All content', async () => {
    expect(responses.allGroupsSubstring.body).toEqual(allGroupsSubstringBgJson);
  });
});

describe('Test selected group (ID = 19) with associations', () => {
  test("Status code", () => {
    expect(responses.selectedGroup.statusCode).toBe(200);
  });
  test('Basic info', async () => {
    expect(responses.selectedGroup.body.id).toBe(19);
    expect(responses.selectedGroup.body.uid).toBe('fcm_edit_user');
  });
  test('All content', async () => {
    expect(responses.selectedGroup.body).toEqual(id19Json);
  });
});
