const request = require("supertest");
const app = require("../../../app");

const allUsersJson = require('./json/all-users');
const allUsersSubstringIJson = require('./json/all-users-substring-i');
const id2Json = require('./json/single-user-2-all');

const responses = {
  allUsers: [],
  allUsersSubstring: [],
  selectedUser: {}
};

beforeAll(async () => {
  responses.allUsers = await request(app).get("/api/users");
  responses.allUsersSubstring = await request(app).get("/api/users/substring/i");
  responses.selectedUser = await request(app).get('/api/user/2/all-relations')
});

describe('Test all users', () => {
  test("Status code", () => {
    expect(responses.allUsers.statusCode).toBe(200);
  });
  test('Number of users', () => {
    expect(responses.allUsers.body.length).toBe(11);
  });
  test('All content', async () => {
    expect(responses.allUsers.body).toEqual(allUsersJson);
  });
});

describe('Test all users with name containing i', () => {
  test("Status code", () => {
    expect(responses.allUsersSubstring.statusCode).toBe(200);
  });
  test('Number of users', () => {
    expect(responses.allUsersSubstring.body.length).toBe(7);
  });
  test('All content', async () => {
    expect(responses.allUsersSubstring.body).toEqual(allUsersSubstringIJson);
  });
});

describe('Test selected user (ID = 2) with all associations', () => {
  test("Status code", () => {
    expect(responses.selectedUser.statusCode).toBe(200);
  });
  test('Basic info', async () => {
    expect(responses.selectedUser.body.user_id).toBe(2);
    expect(responses.selectedUser.body.first_name).toBe('Simon');
    expect(responses.selectedUser.body.last_name).toBe('Prochazka');
  });
  test('All content', async () => {
    expect(responses.selectedUser.body).toEqual(id2Json);
  });
});
