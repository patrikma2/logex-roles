const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  dialect: 'mssql',
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  logging: (process.env.DB_LOGGING === 'true') ? console.log : false,
  define: {
    schema: 'auth'
  },
  dialectOptions: {
    options: {
      validateBulkLoadParameters: true
    }
  }
});

module.exports = sequelize;
