# Visualisation of user roles used in Logex tools

This app allows you to quickly find all necessary information about users, roles and groups in Logex tools.

## How to run the app

To run the app locally, you just need to import the database, build the Angular app and run the Node.js server.

### 1. Import the database

Use the provided `.bak` file to import the database with mock data to Microsoft SQL Server.

### 2. Create and configure a .env file

Create a `.env` file based on `.env.example` and replace the example data with a real configuration to connect to the
imported database. Please use SQL Server authentication for connection, Windows authentication requires an extra 
module.

### 3. Build the Angular app

Go to `./app` directory, install node dependencies using `npm install` and build the app (optimally to production mode) with
 `ng build --prod`. The final app will be located in `./public` directory

### 4. Run the Node.js server

Finally, go back to the root directory, install node dependencies using `npm install` and run the Node.js server 
with `npm start`. The server should be listening on localhost:3000.
