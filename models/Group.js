const sequelize = require('./../config/database');
const DataTypes = require('sequelize');

const Group = sequelize.define('Groups', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  uid: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  group_description: {
    type: DataTypes.STRING,
    allowNull: false
  },
  parent_group_id: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  product_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
}, {
  timestamps: false
});


module.exports = Group;