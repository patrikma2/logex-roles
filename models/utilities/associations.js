const User = require('../User');
const Role = require('../Role');
const Group = require('../Group');
const Product = require('../Product');

/**
 * Registers associations between Models
 */
const registerAssociations = () => {

  // Group : Product (1:M)
  Group.belongsTo(Product, {
    foreignKey: 'product_id',
    as: 'product'
  });
  Product.hasMany(Group, {
    foreignKey: 'product_id'
  });

  // Role : Product (1:M)
  Role.belongsTo(Product, {
    foreignKey: 'product_id',
    as: 'product'
  });
  Product.hasMany(Role, {
    foreignKey: 'product_id'
  });

  // User : Role (M:N)
  User.belongsToMany(Role, {
    through: 'UserRoles',
    foreignKey: 'user_id',
    timestamps: false,
    as: 'roles_direct'
  });
  Role.belongsToMany(User, {
    through: 'UserRoles',
    foreignKey: 'role_id',
    timestamps: false,
    as: 'users_direct'
  });

  // User : Group (M:N)
  User.belongsToMany(Group, {
    through: 'UserGroups',
    foreignKey: 'user_id',
    timestamps: false,
    as: 'groups_direct'
  });
  Group.belongsToMany(User, {
    through: 'UserGroups',
    foreignKey: 'group_id',
    timestamps: false,
    as: 'users_direct'
  });

  // Group : Role (M:N)
  Group.belongsToMany(Role, {
    through: 'GroupRoles',
    foreignKey: 'group_id',
    timestamps: false,
    as: 'roles_direct'
  });

  Role.belongsToMany(Group, {
    through: 'GroupRoles',
    foreignKey: 'role_id',
    timestamps: false,
    as: 'groups_direct'
  });
};

module.exports = registerAssociations;