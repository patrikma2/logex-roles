const sequelize = require('./../config/database');
const DataTypes = require('sequelize');

const Role = sequelize.define('Roles', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  uid: {
    type: DataTypes.STRING,
    allowNull: false
  },
  role_description: {
    type: DataTypes.STRING,
    allowNull: true
  },
  product_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
}, {
  timestamps: false
});

module.exports = Role;
