const sequelize = require('./../config/database');
const DataTypes = require('sequelize');

const Product = sequelize.define('Products', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  uid: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  product_description: {
    type: DataTypes.STRING,
    allowNull: true
  },
  abbreviation: {
    type: DataTypes.STRING,
    allowNull: true
  },
  role_prefix: {
    type: DataTypes.STRING,
    allowNull: true
  },
}, {
  timestamps: false
});

module.exports = Product;